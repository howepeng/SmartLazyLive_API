/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50544
Source Host           : localhost:3306
Source Database       : smartlazy-account

Target Server Type    : MYSQL
Target Server Version : 50544
File Encoding         : 65001

Date: 2017-04-09 23:52:54
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `account_user`
-- ----------------------------
DROP TABLE IF EXISTS `account_user`;
CREATE TABLE `account_user` (
  `user_id` varchar(36) NOT NULL,
  `user_account` varchar(15) NOT NULL,
  `pwd` varchar(40) NOT NULL,
  `name` varchar(40) DEFAULT NULL,
  `last_login_date` datetime NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `upper_id` varchar(36) DEFAULT NULL,
  `token` varchar(40) DEFAULT NULL,
  `series_number` varchar(40) DEFAULT NULL,
  `point` decimal(10,2) NOT NULL DEFAULT '0.00',
  `del_flag` varchar(1) NOT NULL,
  `create_time` datetime NOT NULL,
  `create_user` varchar(36) NOT NULL,
  `update_time` datetime NOT NULL,
  `update_user` varchar(36) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of account_user
-- ----------------------------
INSERT INTO `account_user` VALUES ('3a808252-1cd0-473f-aa88-ad472c477426', '13111111111', '96e79218965eb72c92a549dd5a330112', '测试', '2016-05-25 08:59:52', '0', null, null, null, '0.00', '1', '2016-05-25 08:53:22', '3a808252-1cd0-473f-aa88-ad472c477426', '2016-06-15 14:43:37', 'df021530-3488-42e5-973a-40f3b1b42cea');
