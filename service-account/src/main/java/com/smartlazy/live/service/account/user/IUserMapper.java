/**
 * Project Name:service-account
 * File Name:IUserMapper.java
 * Package Name:com.smartlazy.live.service.account.user
 * Date:2017年2月2日下午10:53:44
 * Author：howepeng
 * Copyright (c) 2017, smartlazy All Rights Reserved.
 *
 */

package com.smartlazy.live.service.account.user;

import org.apache.ibatis.annotations.Param;

import com.smartlazy.live.service.account.mybatis.BaseMapper;

/**
 * ClassName:IUserMapper <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:   TODO ADD REASON. <br/>
 * Date:     2017年2月2日 下午10:53:44 <br/>
 * @author   howepeng
 * @version
 * @since    JDK 1.7
 * @see
 */
public interface IUserMapper extends BaseMapper {

    /**
     *
     * exists:查找用户是否存在. <br/>
     *
     * @author howepeng
     * @param userAccount 账号
     * @return true：存在
     *         false：不存在
     * @throws RuntimeException
     * @since JDK 1.7
     */
    public Boolean exists(@Param("userAccount")String userAccount) throws RuntimeException;
}

