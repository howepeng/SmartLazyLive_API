/**
 * Project Name:service-account
 * File Name:UserServiceImpl.java
 * Package Name:com.smartlazy.live.service.account.user
 * Date:2017年1月5日下午10:54:59
 * Author：howepeng
 * Copyright (c) 2017, smartlazy All Rights Reserved.
 *
 */

package com.smartlazy.live.service.account.user;

import javax.annotation.Resource;

import com.alibaba.dubbo.config.annotation.Service;

/**
 * ClassName:UserServiceImpl <br/>
 * Function: 用户操作. <br/>
 * Reason:   用户操作业务逻辑实现. <br/>
 * Date:     2017年1月5日 下午10:54:59 <br/>
 * @author   howepeng
 * @version
 * @since    JDK 1.7
 * @see
 */
@Service(version="1.0.0")
public class UserServiceImpl implements UserService {
    @Resource
    private IUserMapper userMapper;

    @Override
    public String login(String userAccount) {
        System.out.println("userAccount is === " + userAccount);
        if (userMapper.exists(userAccount)) {
            return "用户已经存在------------ >>>> " + userAccount;
        }
        return "用户不存在 ------------- >>>> " + userAccount;
    }
}

