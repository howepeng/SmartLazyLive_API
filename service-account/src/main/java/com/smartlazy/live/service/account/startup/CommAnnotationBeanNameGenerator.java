/**
 * Project Name:service-account
 * File Name:CommAnnotationBeanNameGenerator.java
 * Package Name:com.smartlazy.live.service.account.startup
 * Date:2017年1月5日下午11:03:29
 * Author：howepeng
 * Copyright (c) 2017, smartlazy All Rights Reserved.
 *
 */

package com.smartlazy.live.service.account.startup;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.AnnotationBeanNameGenerator;

/**
 * ClassName:CommAnnotationBeanNameGenerator <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:   TODO ADD REASON. <br/>
 * Date:     2017年1月5日 下午11:03:29 <br/>
 * @author   howepeng
 * @version
 * @since    JDK 1.7
 * @see
 */
public class CommAnnotationBeanNameGenerator extends AnnotationBeanNameGenerator {
    /**
     * 务相关的bean不用写bean的名称了，ApplicationContext.getBean("类的全路径")就可以得到类的实例了。
     * @see org.springframework.context.annotation.AnnotationBeanNameGenerator#buildDefaultBeanName(org.springframework.beans.factory.config.BeanDefinition)
     */
    @Override
    protected String buildDefaultBeanName(BeanDefinition definition) {
        return definition.getBeanClassName();
    }
}

