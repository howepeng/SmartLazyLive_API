/**
 * Project Name:service-account
 * File Name:UserService.java
 * Package Name:com.smartlazy.live.service.account.user
 * Date:2017年1月5日下午10:51:43
 * Author：howepeng
 * Copyright (c) 2017, smartlazy All Rights Reserved.
 *
 */

package com.smartlazy.live.service.account.user;

/**
 * ClassName:UserService <br/>
 * Function: 用户操作. <br/>
 * Reason:   用户操作业务逻辑接口. <br/>
 * Date:     2017年1月5日 下午10:51:43 <br/>
 * @author   howepeng
 * @version
 * @since    JDK 1.7
 * @see
 */
public interface UserService {
    /**
     *
     * login:登录. <br/>
     *
     * @author howepeng
     * @param name
     * @return
     * @since JDK 1.7
     */
    public String login(String userAccount);
}

