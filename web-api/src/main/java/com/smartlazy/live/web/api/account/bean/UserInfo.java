/**
 * Project Name:web-api
 * File Name:UserInfo.java
 * Package Name:com.smartlazy.live.web.api.account.bean
 * Date:2016年12月14日下午11:18:52
 * Author：howepeng
 * Copyright (c) 2016, smartlazy All Rights Reserved.
 *
 */

package com.smartlazy.live.web.api.account.bean;

/**
 * ClassName:UserInfo <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:   TODO ADD REASON. <br/>
 * Date:     2016年12月14日 下午11:18:52 <br/>
 * @author   howepeng
 * @version
 * @since    JDK 1.7
 * @see
 */
public class UserInfo {
    private Long id;
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserInfo user = (UserInfo) o;

        if (id != null ? !id.equals(user.id) : user.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
