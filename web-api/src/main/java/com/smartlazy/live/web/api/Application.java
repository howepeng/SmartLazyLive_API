/**
 * Project Name:web-api
 * File Name:Application.java
 * Package Name:com.smartlazy.live.web.api
 * Date:2016年12月14日下午11:18:52
 * Author：howepeng
 * Copyright (c) 2016, smartlazy All Rights Reserved.
 *
 */

package com.smartlazy.live.web.api;

import java.util.HashSet;
import java.util.Set;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * ClassName:Application <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:   TODO ADD REASON. <br/>
 * Date:     2016年12月14日 下午11:18:52 <br/>
 * @author   howepeng
 * @version
 * @since    JDK 1.7
 * @see
 */
@Configuration
@EnableAutoConfiguration
@SpringBootApplication
@ImportResource("classpath:dubbo-consumer.xml")
@ComponentScan(basePackages = {"com.smartlazy.live"})
public class Application extends SpringBootServletInitializer implements EmbeddedServletContainerCustomizer {

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(Application.class);
        app.setWebEnvironment(true);
        Set<Object> set = new HashSet<Object>();
        app.setSources(set);
        app.run(args);
    }

    @Override
    public void customize(ConfigurableEmbeddedServletContainer container) {
        container.setPort(8081);
    }
}

