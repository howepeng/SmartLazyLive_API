/**
 * Project Name:web-api
 * File Name:AccountController.java
 * Package Name:com.smartlazy.live.web.api.account.controller
 * Date:2016年12月14日下午11:18:52
 * Author：howepeng
 * Copyright (c) 2016, smartlazy All Rights Reserved.
 *
 */

package com.smartlazy.live.web.api.account.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.smartlazy.live.service.account.user.UserService;
import com.smartlazy.live.web.api.account.bean.UserInfo;

/**
 * ClassName:AccountController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:   TODO ADD REASON. <br/>
 * Date:     2016年12月14日 下午11:18:52 <br/>
 * @author   howepeng
 * @version
 * @since    JDK 1.7
 * @see
 */
@RestController
@RequestMapping("/account")
public class AccountController {

    @Autowired
    private UserService userService;

    @RequestMapping("/{id}")
    public UserInfo view(@PathVariable("id") Long id) {
        UserInfo user = new UserInfo();
        user.setId(id);
        user.setName(userService.login(String.valueOf(id)));
        return user;
    }
}
